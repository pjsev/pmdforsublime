import sublime, sublime_plugin
import re

class SampleselectionCommand(sublime_plugin.WindowCommand):
	def run(self):
		print('selection_plugin__called')
		window = self.window
		view = window.active_view()
		sel = view.sel()

		region1 = sel[0]
		selectionText = view.line(region1)
		selectedLine = view.substr(selectionText)
		print ("line: " + selectedLine)
		print (selectionText)
		selectionTextWord = view.word(region1)
		print (view.substr(selectionTextWord))

		#fff = view.find("sub", 0, sublime.LITERAL)
		#print (fff)
		#print (view.substr(fff))
		filePath = re.search('[a-zA-Z]:.*\.cls', selectedLine)
		lineNum = re.search(':[1-9]*[0-9]:', selectedLine)
		formattedLineNumber = lineNum.group(0).replace(":","")
		print ("FILE PATH: " + filePath.group(0))
		print ("LINE NUMBER: " + formattedLineNumber)

		window.open_file(filePath.group(0)+":"+formattedLineNumber, sublime.ENCODED_POSITION)



		
