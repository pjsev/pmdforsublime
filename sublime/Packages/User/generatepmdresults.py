import sublime, sublime_plugin

class GeneratepmdresultsCommand(sublime_plugin.TextCommand):
	def run(self, edit, **args):	
		settings = sublime.load_settings("Preferences.sublime-settings")
		pmdfile = open(settings.get('apexCodeScan_results_path')+'\\pmd_results.txt','r')
		cur_view = sublime.active_window().active_view()
		if args['resulttype'] == "console":
			writefile = pmdfile.read()			
			print(writefile)		
		if args['resulttype'] == "newtab":
			view = self.view.window().new_file()
			cur_view = sublime.active_window().active_view()			
			writefile = pmdfile.read()
			view.insert(edit, 0, writefile)

