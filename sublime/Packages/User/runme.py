import sublime, sublime_plugin
import os
import time

class RunmeCommand(sublime_plugin.WindowCommand):
	def run(self, **kwargs):		
		try:
			settings = sublime.load_settings("Preferences.sublime-settings")
			bpath = settings.get('apexCodeScan_batch_path')
			binpath = settings.get('apexCodeScan_bin_path')
			resultspath = settings.get('apexCodeScan_results_path')
			print(bpath)
			# Get the file name of the current active tab and pass as parameters to the pmd batch
			if "$file_name" in kwargs["cmd"]:
				cur_view = self.window.active_view()								
				cmd_string=kwargs["cmd"].replace("$file_name",cur_view.file_name())
				fullArgs = cmd_string.replace("$batch_path",bpath)				
				os.system("start "+ fullArgs + ' ' + binpath + ' ' + resultspath)
				# Generate an html file of the PMD results
				if "html" in kwargs["cmd"]:
					print("HTML file generated - pmd_results.html")
				# Generate a new tab with the PMD results
				if "newtab" in kwargs["cmd"]:
					print("Generating Results to new tab...")
					time.sleep(3)
					#view = sublime.active_window().new_file()					
					cur_view.run_command("generatepmdresults", {"resulttype":"newtab"})
					print("Text file generated - pmd_results.txt")
				# Generate PMD results in console
				if "sublimeconsole" in kwargs["cmd"]:			
					#cur_view = sublime.active_window().active_view()				
					print("Generating Results to console...")
					time.sleep(3)
					sublime.active_window().run_command("show_panel", {"panel": "console", "toggle": True})
					cur_view.run_command("generatepmdresults", {"resulttype":"console"})					
					print("Text file generated - pmd_results.txt")					
			else:
				os.system(cmd_string)
		except TypeError:
			sublime.active_window().run_command("show_panel", {"panel": "console", "toggle": True})
			sublime.message_dialog("Something went wrong with the command given...\n\n\n"+traceback.format_exc())


