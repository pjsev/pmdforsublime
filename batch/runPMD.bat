cd %3
if "%2" == "newtab" (start /wait call pmd -d %1 -f text -R rulesets/java/apex_ruleset.xml ^> %4/pmd_results.txt ^& exit)
if "%2" == "sublimeconsole" (start /wait call pmd -d %1 -f text -R rulesets/java/apex_ruleset.xml ^> %4/pmd_results.txt ^& exit)
if "%2" == "html" (start /wait call pmd -d %1 -f html -R rulesets/java/apex_ruleset.xml ^> %4/pmd_results.html ^& exit)
if "%2" == "html" (%4/pmd_results.html && exit)
exit



