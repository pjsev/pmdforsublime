Applying Apex Code Scan using Sublime
	
* Copy the runPMD.bat file from the sublime/batch folder and paste it at your desired location
* Go to the user menus of sublime, usually ends in ."sublime-menu" (Preferences > Browse Packages > Mavensmate > sublime > menus
* Edit the menu to include Apex Code Scan options. Add the following snippet to the Context.sublime-menu:
	

	{ "caption": "Apex PMD Code Scan",
			  
		"children":
			  
		[
			{ "caption": "Generate HTML", "command": "runme", "args": {"cmd": "$batch_path $file_name html"} },
			{ "caption": "Generate Console", "command": "runme", "args": {"cmd": "$batch_path $file_name sublimeconsole"} },
			{ "caption": "Generate New Tab", "command": "runme", "args": {"cmd": "$batch_path $file_name newtab"} },
			{ "caption": "Go To File", "command": "sampleselection" },
		]
			
	},

	- Update your User Settings: Under Sublime main menu, click Preferences > Settings - User
	- Add and edit these new settings (double backslash for windows):
	
		"apexCodeScan_batch_path": "<path to your batch file>\\runPMD",
		"apexCodeScan_results_path": "<path to your results folder>",
		"apexCodeScan_bin_path": "<path to your PMD bin folder>"
		
		Examples: ("apexCodeScan_batch_path": "C:\\Users\\Lenovo\\Documents\\Projects\\PMD\\runPMD",
					"apexCodeScan_results_path": "C:\\Users\\Lenovo\\Documents\\Projects\\PMD\\pmd-bin-5.4.1\\bin")
		
	- Paste the .py files from the sublime folder into your Sublime plugins root folder...or under sublime, click Preferences > Browse Packages > User folder. 
		usuallu .py files are located there.
	- Test the code scan by right-clicking on any Apex Class file and checking the 3 options listed (HTML, Console, New Tab)